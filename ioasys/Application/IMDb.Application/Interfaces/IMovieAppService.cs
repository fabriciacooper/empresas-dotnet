﻿using IMDb.Application.ViewModels;
using IMDb.Domain.Models;

namespace IMDb.Application.Interfaces
{
    public interface IMovieAppService : IAppServiceBase<Movie, MovieViewModel>
    {
        PagedListViewModel<MovieViewModel> GetMovies(PaginationParameters paginationParameters, FilterParameters filterParameters);
        bool AddMovieClassification(MovieClassificationViewModel viewModel);
    }
}
