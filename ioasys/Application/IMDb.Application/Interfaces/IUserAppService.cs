﻿using IMDb.Application.ViewModels;
using IMDb.Domain.Models;

namespace IMDb.Application.Interfaces
{
    public interface IUserAppService : IAppServiceBase<User, UserViewModel>
    {
        object Authentication(UserViewModel user); 
    }
}
