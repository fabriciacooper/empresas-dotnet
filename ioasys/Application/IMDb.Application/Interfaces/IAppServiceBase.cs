﻿using IMDb.Application.ViewModels;
using IMDb.Domain.Models;
using System;
using System.Collections.Generic;

namespace IMDb.Application.Interfaces
{
    public interface IAppServiceBase<TEntity, TBaseViewModel> where TEntity : Entity where TBaseViewModel : BaseViewModel
    {
        void Add(TBaseViewModel entidade);
        void Delete(Guid id);
        void Delete(TBaseViewModel entidade);
        void Update(TBaseViewModel entidade);
        TBaseViewModel GetById(Guid id);
        IEnumerable<TBaseViewModel> GetAll();
    }
}
