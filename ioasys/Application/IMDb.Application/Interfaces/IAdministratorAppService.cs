﻿using IMDb.Application.ViewModels;
using IMDb.Domain.Models;

namespace IMDb.Application.Interfaces
{
    public interface IAdministratorAppService : IAppServiceBase<User, AdministratorViewModel>
    {
    }
}
