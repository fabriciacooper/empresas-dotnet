﻿using AutoMapper;
using IMDb.Application.ViewModels;
using IMDb.Domain.Models;

namespace IMDb.Application.Mapping
{
    public class ViewModelToDomain : Profile
    {
        public ViewModelToDomain()
        {
            CreateMap<UserViewModel, User>();
            CreateMap<AdministratorViewModel, User>();
            CreateMap<MovieViewModel, Movie>();
            CreateMap<ActorViewModel, Actor>();
            CreateMap<MovieClassificationViewModel, MovieClassification>();
        }
    }
}
