﻿using AutoMapper;
using IMDb.Application.ViewModels;
using IMDb.Domain.Models;

namespace IMDb.Application.Mapping
{
    public class DomainToViewModel : Profile
    {
        public DomainToViewModel()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<User, AdministratorViewModel>();
            CreateMap<Movie, MovieViewModel>();
            CreateMap<Actor, ActorViewModel>();
            CreateMap<MovieClassification, MovieClassificationViewModel>();
        }
    }
}
