﻿using AutoMapper;

namespace IMDb.Application.Mapping
{
    public class AutoMapperConfiguration
    {
        public MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(ps =>
            {
                ps.AddProfile(new DomainToViewModel());
                ps.AddProfile(new ViewModelToDomain());
            });
        }
    }
}
