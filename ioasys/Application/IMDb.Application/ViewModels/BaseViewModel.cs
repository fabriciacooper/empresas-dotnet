﻿using System;

namespace IMDb.Application.ViewModels
{
    public class BaseViewModel
    {
        public Guid Id { get; set; }
    }
}
