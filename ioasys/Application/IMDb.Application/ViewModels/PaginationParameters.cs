﻿namespace IMDb.Application.ViewModels
{
	public class PaginationParameters
    {
		private int _pageSize = 10;
		private const int maxPageSize = 50;

		public int PageNumber { get; set; } = 1;		
		public int PageSize
		{
			get
			{
				return _pageSize;
			}
			set
			{
				_pageSize = (value > maxPageSize) ? maxPageSize : value;
			}
		}
	}
}
