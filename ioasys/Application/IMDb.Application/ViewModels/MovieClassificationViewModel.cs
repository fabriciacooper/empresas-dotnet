﻿using IMDb.Domain.Enum;
using System;

namespace IMDb.Application.ViewModels
{
    public class MovieClassificationViewModel : BaseViewModel
    {
        public Guid MovieId { get; set; }
        public MovieViewModel Movie { get; set; }

        public Guid UserId { get; set; }
        public UserViewModel User { get; set; }

        public MovieScale Vote { get; set; }
    }
}
