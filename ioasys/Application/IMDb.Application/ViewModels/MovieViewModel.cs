﻿using IMDb.Domain.Enum;
using System;
using System.Collections.Generic;

namespace IMDb.Application.ViewModels
{
    public class MovieViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public DateTime MovieRelease { get; set; }
        public Genres Genres { get; set; }
        public bool Status { get; set; }
        public decimal Media { get; set; }
        public List<ActorViewModel> Actors { get; set; }

        public MovieViewModel()
        {
            Actors = new List<ActorViewModel>();
        }
    }
}
