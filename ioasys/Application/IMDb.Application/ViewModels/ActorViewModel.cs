﻿namespace IMDb.Application.ViewModels
{
    public class ActorViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
