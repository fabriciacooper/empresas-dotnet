﻿namespace IMDb.Application.ViewModels
{
    public class AdministratorViewModel : BaseViewModel
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool Status { get; set; }
    }
}
