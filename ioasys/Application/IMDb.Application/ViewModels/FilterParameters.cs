﻿using IMDb.Domain.Enum;
using System.Collections.Generic;

namespace IMDb.Application.ViewModels
{
    public class FilterParameters
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public Genres Genres { get; set; }
        public List<ActorViewModel> Actors { get; set; }

        public FilterParameters()
        {
            Actors = new List<ActorViewModel>();
        }
    }
}
