﻿using AutoMapper;
using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;
using IMDb.ManagerAuthetication;

namespace IMDb.Application.Services
{
    public class UserAppService : AppServiceBase<User, UserViewModel>, IUserAppService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        public UserAppService(IMapper mapper, IUserService service) : base(mapper, service)
        {
            _mapper = mapper;
            _userService = service;
        }

        public object Authentication(UserViewModel userViewModel)
        {
            var entityUser = _mapper.Map<User>(userViewModel);
            var userService = _userService.GetUser(entityUser.Email, entityUser.Password);

            if (userService == null)
                return null;

            var token = TokenService.GenerateToken(userService);
            return new
            {
                accessToken = token
            };
        }
    }
}
