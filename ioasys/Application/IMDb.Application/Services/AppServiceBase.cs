﻿using AutoMapper;
using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;
using System;
using System.Collections.Generic;

namespace IMDb.Application.Services
{
    public class AppServiceBase<TEntity, TBaseViewModel> : IAppServiceBase<TEntity, TBaseViewModel>
        where TEntity : Entity
        where TBaseViewModel : BaseViewModel
    {
        private readonly IServiceBase<TEntity> _service;
        private readonly IMapper _mapper;

        public AppServiceBase(IMapper mapper, IServiceBase<TEntity> service) : base()
        {
            _service = service;
            _mapper = mapper;
        }

        public void Add(TBaseViewModel entidade)
        {
            var viewModel = _mapper.Map<TEntity>(entidade);
            _service.Add(viewModel);
        }

        public void Delete(Guid id)
        {
            _service.Delete(id);
        }

        public void Delete(TBaseViewModel entidade)
        {
            var viewModel = _mapper.Map<TEntity>(entidade);
            _service.Delete(viewModel);
        }

        public IEnumerable<TBaseViewModel> GetAll()
        {
            var data = _service.GetAll();
            var viewModel = _mapper.Map<IEnumerable<TBaseViewModel>>(data);

            return viewModel;
        }

        public TBaseViewModel GetById(Guid id)
        {
            var data = _service.GetById(id);
            var viewModel = _mapper.Map<TBaseViewModel>(data);

            return viewModel;
        }

        public void Update(TBaseViewModel entidade)
        {
            var viewModel = _mapper.Map<TEntity>(entidade);
            _service.Update(viewModel);
        }
    }
}
