﻿using AutoMapper;
using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Application.Services
{
    public class MovieAppService : AppServiceBase<Movie, MovieViewModel>, IMovieAppService
    {
        private readonly IMovieService _movieAppService;
        private readonly IMapper _mapper;

        public MovieAppService(IMapper mapper, IMovieService service) : base(mapper, service)
        {
            _mapper = mapper;
            _movieAppService = service;
        }

        public bool AddMovieClassification(MovieClassificationViewModel viewModel)
        {
            var movie = _movieAppService.GetById(viewModel.MovieId);
            if (movie == null)
                return false;

            var movieClassification = _mapper.Map<MovieClassification>(viewModel);
            _movieAppService.AddMovieClassification(movieClassification);

            return true;
        }

        public PagedListViewModel<MovieViewModel> GetMovies(PaginationParameters paginationParameters, FilterParameters filterParameters)
        {
            var movies = _movieAppService.GetMoviesWithClassification();
            
            if (!string.IsNullOrWhiteSpace(filterParameters.Director))
                movies = movies.Where(c => c.Director.ToLower().Contains(filterParameters.Director.ToLower())).ToList();

            if (!string.IsNullOrWhiteSpace(filterParameters.Name))
                movies = movies.Where(c => c.Name.ToLower().Contains(filterParameters.Name.ToLower())).ToList();

            if (filterParameters.Genres > 0)
                movies = movies.Where(c => c.Genres == filterParameters.Genres).ToList();

            foreach (var movie in movies)
            {
                var media = movie.MoviesClassification.Sum(x => Convert.ToDecimal(x.Vote));
                movie.Media = media / movie.MoviesClassification.Count();
            }

            var movieViewModel = _mapper.Map<List<MovieViewModel>>(movies);

            var pageList = PagedListViewModel<MovieViewModel>.ToPagedList(movieViewModel.OrderBy(o => o.Name), 
                paginationParameters.PageNumber, paginationParameters.PageSize);

            return pageList;
        } 
    }
}
