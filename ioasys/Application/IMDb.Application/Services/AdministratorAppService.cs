﻿using AutoMapper;
using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;

namespace IMDb.Application.Services
{
    public class AdministratorAppService : AppServiceBase<User, AdministratorViewModel>, IAdministratorAppService
    {
        public AdministratorAppService(IMapper mapper, IServiceBase<User> service) : base(mapper, service)
        {
        }
    }
}
