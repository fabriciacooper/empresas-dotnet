﻿using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace IMDb.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "administrator, user")]
    public class UserController : ControllerBase
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        /// <summary>
        /// Adicionar usuário
        /// </summary>
        /// <param name="viewModel">Dados do usuário</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] UserViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid");
           
            _userAppService.Add(viewModel);
            return Ok();
        }

        /// <summary>
        /// Atualizar usuário
        /// </summary>
        /// <param name="viewModel">Dados do usuário</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody] UserViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid");
           
            var user = _userAppService.GetById(viewModel.Id);

            if(user == null)
                return BadRequest("User not found.");

            _userAppService.Update(user);
            return Ok();
        }

        /// <summary>
        /// Obter todos usuários ativos
        /// </summary>        
        /// <returns>Retorna uma lista de usuários</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _userAppService.GetAll().Where(x => x.Status == true && x.Role != "administrator");
            return Ok(result);
        }

        /// <summary>
        /// Obter usuário por id
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna uma lista de usuários</returns>
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            var result = _userAppService.GetById(id);

            if(result == null)
                return BadRequest("User not found.");

            return Ok(result);
        }

        /// <summary>
        /// Desativar usuário
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns></returns>
        [HttpPut("Delete/{id:Guid}")] 
        public IActionResult Delete(Guid id)
        {
            var user = _userAppService.GetById(id);

            if (user == null)
                return BadRequest("User not found.");

            user.Status = false;
            _userAppService.Update(user);

            return Ok();
        }
    }
}