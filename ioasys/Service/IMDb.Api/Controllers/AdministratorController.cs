﻿using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace IMDb.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "administrator")]
    public class AdministratorController : ControllerBase
    {
        private readonly IAdministratorAppService _admAppService;

        public AdministratorController(IAdministratorAppService admAppService)
        {
            _admAppService = admAppService;
        }

        /// <summary>
        /// Adicionar usuários administradores
        /// </summary>
        /// <param name="viewModel">Dados do usuário a ser inserido</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] AdministratorViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid");

            _admAppService.Add(viewModel);
            return Ok();
        }

        /// <summary>
        /// Atualizar usuário administradores
        /// </summary>
        /// <param name="viewModel">Dados do usuário a ser atualizado</param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody] AdministratorViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid");

            var adm = _admAppService.GetById(viewModel.Id);

            if (adm == null)
                return BadRequest("Administrador not found.");

            _admAppService.Update(adm);
            return Ok();
        }

        /// <summary>
        /// Obter todos os usuários que não administradores
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _admAppService
                .GetAll()
                .Where(u => u.Status == true && u.Role == "user")
                .OrderBy(o => o.FirstName);

            return Ok(users);
        }

        /// <summary>
        /// Obter administrador por id
        /// </summary>
        /// <param name="id">Id do administrador</param>
        /// <returns>Administrador</returns>
        [HttpGet("{id:Guid}/Administrator")]
        public IActionResult Get(Guid id)
        {
            var result = _admAppService.GetById(id);

            if (result == null)
                return BadRequest("Administrador not found");

            return Ok(result);
        }

        /// <summary>
        /// Obter usuário por id
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Usuário</returns>
        [HttpGet("{id:Guid}/User")]
        public IActionResult GetUsers(Guid id)
        {
            var result = _admAppService.GetById(id);

            if (result == null)
                return BadRequest("User not found");

            return Ok(result);
        }

        /// <summary>
        /// Desativar um usuário
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns></returns>
        [HttpPut("Delete/{id:Guid}")]
        public IActionResult Delete(Guid id)
        {
            var adm = _admAppService.GetById(id);

            if (adm == null)
                return BadRequest("User not found.");

            adm.Status = false;
            _admAppService.Update(adm);

            return Ok();
        }

    }
}