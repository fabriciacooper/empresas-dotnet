﻿using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IMDb.Api.Controllers
{
    [ApiController]
    [Route("api/login")]
    public class LoginController : ControllerBase
    {
        private readonly IUserAppService _userAppService;

        public LoginController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="user">Dados do usuário para se autenticação. Deve passar apenas o email e senha</param>
        /// <returns>token</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] UserViewModel user)
        {
            var result = _userAppService.Authentication(user);
            if (result == null)
                return BadRequest(new {message = "Usuário não encontrado"});

            return result;           
        }

        /// <summary>
        /// Usuário sem permissão
        /// </summary>        
        /// <returns>Uma mensagem</returns>
        [AllowAnonymous]
        [HttpGet("anonymous")]
        public string Anonymous() => "Anônimo";

        /// <summary>
        /// Usuário com permissão
        /// </summary>        
        /// <returns>Retorna o guid do usuário autenticado</returns>
        [HttpGet("authenticated")]
        [Authorize]
        public string Authenticated() => string.Format("Autenticado - {0}", User.Identity.Name);

        /// <summary>
        /// Usuário com permissão administrador
        /// </summary>        
        /// <returns>Uma mensagem</returns>
        [HttpGet("administrator")]
        [Authorize(Roles = "administrator")]
        public string Administrator() => "Administrator";

        /// <summary>
        /// Usuário com permissão usuário comum
        /// </summary>        
        /// <returns>Uma mensagem</returns>
        [HttpGet("user")]
        [Authorize(Roles = "user")]
        public string CommonUser() => "User";
    }
}