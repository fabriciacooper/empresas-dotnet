﻿using IMDb.Application.Interfaces;
using IMDb.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace IMDb.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "administrator, user")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieAppService _movieAppService;

        public MovieController(IMovieAppService movieAppService)
        {
            _movieAppService = movieAppService;
        }

        /// <summary>
        /// Adicionar filme
        /// </summary>
        /// <param name="viewModel">Dados do filme</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] MovieViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid");

            _movieAppService.Add(viewModel);
            return Ok();
        }

        /// <summary>
        /// Votação para avaliar um filme
        /// </summary>
        /// <param name="viewModel">Dados para avaliar o filme</param>
        /// <returns></returns>
        [HttpPost]
        [Route("vote")]
        public IActionResult Vote([FromBody] MovieClassificationViewModel viewModel)
        {
            viewModel.UserId = new Guid(User.Identity.Name);
            var add = _movieAppService.AddMovieClassification(viewModel);

            if (!add)
                return BadRequest("Invalid");
            
            return Ok();
        }

        /// <summary>
        /// Obter filme por id
        /// </summary>
        /// <param name="id">Id do filme</param>
        /// <returns>Filme</returns>
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            var result = _movieAppService.GetById(id);

            if (result == null)
                return BadRequest("Movie not found.");

            return Ok(result);
        }

        /// <summary>
        /// Obter filmes por filtro
        /// </summary>
        /// <param name="pagination">paginação</param>
        /// <param name="filters">filtros</param>
        /// <returns>Uma lista de filmes</returns>
        [HttpGet("GetMovies")]
        public IActionResult GetMovies([FromQuery] PaginationParameters pagination, [FromBody] FilterParameters filters)
        {
            var pageList = _movieAppService.GetMovies(pagination, filters);

            var metadata = new
            {
                pageList.TotalCount,
                pageList.PageSize,
                pageList.CurrentPage,
                pageList.TotalPages,
                pageList.HasNext,
                pageList.HasPrevious
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(pageList);
        }
    }
}