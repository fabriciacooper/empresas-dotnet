﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Service
{
    public interface IUserService : IServiceBase<User>
    {
        User GetUser(string email, string password);
    }
}
