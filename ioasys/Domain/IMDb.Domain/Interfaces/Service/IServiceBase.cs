﻿using IMDb.Domain.Models;
using System;
using System.Collections.Generic;

namespace IMDb.Domain.Interfaces.Service
{
    public interface IServiceBase<TEntity> where TEntity : Entity
    {
        void Delete(Guid id);
        void Delete(TEntity entity);
        void Add(TEntity entity);
        void Update(TEntity entity);
        TEntity GetById(Guid id);
        IEnumerable<TEntity> GetAll();    
    }
}
