﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Service
{
    public interface IAdministratorService : IServiceBase<User>
    {
    }
}
