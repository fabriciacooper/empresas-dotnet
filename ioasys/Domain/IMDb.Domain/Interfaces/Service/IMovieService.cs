﻿using IMDb.Domain.Models;
using System.Collections.Generic;

namespace IMDb.Domain.Interfaces.Service
{
    public interface IMovieService : IServiceBase<Movie>
    {
        void AddMovieClassification(MovieClassification movieClassification);
        List<Movie> GetMoviesWithClassification();
    }
}
