﻿using IMDb.Domain.Models;
using System.Collections.Generic;

namespace IMDb.Domain.Interfaces.Repository
{
    public interface IMovieRepository : IRepository<Movie>
    {
        void AddMovieClassification(MovieClassification movieClassification);
        List<Movie> GetMoviesWithClassification();
    }
}
