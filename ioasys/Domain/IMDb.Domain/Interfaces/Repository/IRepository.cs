﻿using IMDb.Domain.Models;
using System;
using System.Collections.Generic;

namespace IMDb.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        void Delete(Guid id);
        void Delete(TEntity entity);
        void Add(TEntity entity);
        void Update(TEntity entity);
        TEntity GetById(Guid id);
        IEnumerable<TEntity> GetAll();
    }
}
