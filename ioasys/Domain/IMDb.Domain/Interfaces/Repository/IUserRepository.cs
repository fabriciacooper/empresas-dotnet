﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Repository
{
    public interface IUserRepository: IRepository<User>
    {
        User GetUser(string email, string password);
    }
}
