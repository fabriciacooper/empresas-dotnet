﻿using IMDb.Domain.Models;

namespace IMDb.Domain.Interfaces.Repository
{
    public interface IAdministratorRepository : IRepository<User>
    {
    }
}
