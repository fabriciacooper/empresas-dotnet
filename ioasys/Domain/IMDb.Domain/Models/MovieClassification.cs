﻿using IMDb.Domain.Enum;
using System;

namespace IMDb.Domain.Models
{
    public class MovieClassification : Entity
    {
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public MovieScale Vote { get; set; }
    }
}
