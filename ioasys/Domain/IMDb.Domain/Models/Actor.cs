﻿using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class Actor : Entity
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public List<ActorMovie> ActorMovie { get; set; }

        public Actor()
        {
            ActorMovie = new List<ActorMovie>();
        }
    }
}
