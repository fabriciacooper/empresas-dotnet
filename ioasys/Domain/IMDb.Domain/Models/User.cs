﻿using System.Collections.Generic;

namespace IMDb.Domain.Models
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool Status { get; set; }

        public List<MovieClassification> MoviesClassification { get; set; }

        public User()
        {
            MoviesClassification = new List<MovieClassification>();
        }
    }
}