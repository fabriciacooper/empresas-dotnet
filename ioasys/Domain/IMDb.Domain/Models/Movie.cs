﻿using IMDb.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace IMDb.Domain.Models
{
    public class Movie : Entity
    {
        public string Name { get; set; }
        public string Director { get; set; }        
        public DateTime MovieRelease { get; set; }
        public Genres Genres { get; set; }
        public bool Status { get; set; }
        [NotMapped]
        public decimal Media { get; set; }

        public List<ActorMovie> ActorMovie { get; set; }
        public List<MovieClassification> MoviesClassification { get; set; }

        public Movie()
        {
            ActorMovie = new List<ActorMovie>();
            MoviesClassification = new List<MovieClassification>();
        }
    }
}
