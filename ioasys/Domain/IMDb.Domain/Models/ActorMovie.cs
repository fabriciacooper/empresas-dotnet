﻿using System;

namespace IMDb.Domain.Models
{
    public class ActorMovie : Entity
    {
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }
        public Guid ActorId { get; set; }
        public Actor Actor { get; set; }
    }
}
