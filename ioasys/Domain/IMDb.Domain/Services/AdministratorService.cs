﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;

namespace IMDb.Domain.Services
{
    public class AdministratorService : ServiceBase<User>, IAdministratorService
    {
        public AdministratorService(IRepository<User> repository) : base(repository)
        {
        }
    }
}
