﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;

namespace IMDb.Domain.Services
{
    public class UserService : ServiceBase<User>, IUserService
    {
        private IUserRepository _userRepository;
        public UserService(IUserRepository repository) : base(repository)
        {
            _userRepository = repository;
        }

        public User GetUser(string email, string password)
        {
            var user = _userRepository.GetUser(email, password);
            return user;
        }
    }
}
