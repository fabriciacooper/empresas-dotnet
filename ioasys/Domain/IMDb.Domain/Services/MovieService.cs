﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Models;
using System.Collections.Generic;

namespace IMDb.Domain.Services
{
    public class MovieService : ServiceBase<Movie>, IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository) : base(movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public void AddMovieClassification(MovieClassification movieClassification)
        {
            _movieRepository.AddMovieClassification(movieClassification);
        }

        public List<Movie> GetMoviesWithClassification()
        {
            return _movieRepository.GetMoviesWithClassification();
        }
    }
}
