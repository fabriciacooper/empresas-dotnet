﻿namespace IMDb.Domain.Enum
{
    public enum MovieScale : byte
    {
        Bad = 1,
        Nice = 2,
        VeryGood = 3,
        Amazing = 4
    }
}
