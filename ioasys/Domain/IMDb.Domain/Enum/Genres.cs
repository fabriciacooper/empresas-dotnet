﻿namespace IMDb.Domain.Enum
{
    public enum Genres : byte
    {
        Action = 1,
        Comedy = 2,
        Drama = 3,
        Fantasy = 4,
        Horror = 5,
        Mystery = 6,
        Romance = 7,
        Thriller = 8,
        Western = 9
    }
}
