﻿using IMDb.Application.Interfaces;
using IMDb.Application.Services;
using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Interfaces.Service;
using IMDb.Domain.Services;
using IMDb.Infra.Data.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace IMDb.Infra.IoC
{
    public class InjectorDependency
    {
        public static void RegisterServices(IServiceCollection service)
        {
            service.AddScoped(typeof(IAppServiceBase<,>), typeof(AppServiceBase<,>));
            service.AddScoped<IAdministratorAppService, AdministratorAppService>();
            service.AddScoped<IMovieAppService, MovieAppService>();
            service.AddScoped<IUserAppService, UserAppService>();

            service.AddScoped(typeof(IServiceBase<>), typeof(ServiceBase<>));
            service.AddScoped<IAdministratorService, AdministratorService>();
            service.AddScoped<IMovieService, MovieService>();
            service.AddScoped<IUserService, UserService>();

            service.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            service.AddScoped<IAdministratorRepository, AdministratorRepository>();
            service.AddScoped<IMovieRepository, MovieRepository>();
            service.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
