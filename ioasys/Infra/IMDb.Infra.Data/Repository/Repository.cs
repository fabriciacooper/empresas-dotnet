﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Models;
using IMDb.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly IMDbContext _context;

        public Repository(IMDbContext contexto) : base()
        {
            _context = contexto;
        }

        public void Add(TEntity entity)
        {
            _context.InitializeTransaction();
            _context.Set<TEntity>().Add(entity);
            _context.SendChanges();
        }

        public void Delete(Guid id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                _context.InitializeTransaction();
                _context.Set<TEntity>().Remove(entity);
                _context.SendChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            _context.InitializeTransaction();
            _context.Set<TEntity>().Remove(entity);
            _context.SendChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        public TEntity GetById(Guid id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public void Update(TEntity entity)
        {
            _context.InitializeTransaction();
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SendChanges();
        }
    }
}
