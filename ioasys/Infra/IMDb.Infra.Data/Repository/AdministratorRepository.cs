﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Models;
using IMDb.Infra.Data.Context;

namespace IMDb.Infra.Data.Repository
{
    public class AdministratorRepository : Repository<User>, IAdministratorRepository
    {
        public AdministratorRepository(IMDbContext contexto) : base(contexto)
        {
        }
    }
}
