﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Models;
using IMDb.Infra.Data.Context;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private IMDbContext _context;
        public UserRepository(IMDbContext contexto) : base(contexto)
        {
            _context = contexto;
        }

        public User GetUser(string email, string password)
        {
            var user = _context.Users.Where(x => x.Email == email && x.Password == password).FirstOrDefault();
            return user;
        }
    }
}
