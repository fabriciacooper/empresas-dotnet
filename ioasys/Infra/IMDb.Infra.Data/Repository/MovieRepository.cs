﻿using IMDb.Domain.Interfaces.Repository;
using IMDb.Domain.Models;
using IMDb.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        private readonly IMDbContext _context;
        public MovieRepository(IMDbContext contexto) : base(contexto)
        {
            _context = contexto;
        }

        public void AddMovieClassification(MovieClassification movieClassification)
        {
            _context.MoviesClassification.Add(movieClassification);
            _context.SendChanges();
        }

        public List<Movie> GetMoviesWithClassification()
        {
            var movies = _context.Set<Movie>()
                .Include(x => x.MoviesClassification)
                .ToList();

            return movies;
        }
    }
}
