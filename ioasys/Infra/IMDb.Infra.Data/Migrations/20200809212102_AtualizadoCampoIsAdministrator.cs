﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDb.Infra.Data.Migrations
{
    public partial class AtualizadoCampoIsAdministrator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Use_IsAdministrator",
                table: "Use_User");

            migrationBuilder.AddColumn<string>(
                name: "Use_Role",
                table: "Use_User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Use_Role",
                table: "Use_User");

            migrationBuilder.AddColumn<bool>(
                name: "Use_IsAdministrator",
                table: "Use_User",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}
