﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDb.Infra.Data.Migrations
{
    public partial class CriandoTabelas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Act_Actor",
                columns: table => new
                {
                    Act_ActorId = table.Column<Guid>(nullable: false),
                    Act_Name = table.Column<string>(nullable: true),
                    Act_Age = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Act_Actor", x => x.Act_ActorId);
                });

            migrationBuilder.CreateTable(
                name: "Mov_Movie",
                columns: table => new
                {
                    Mov_MovieId = table.Column<Guid>(nullable: false),
                    Mov_Name = table.Column<string>(nullable: true),
                    Mov_Director = table.Column<string>(nullable: true),
                    Mov_MovieRelease = table.Column<DateTime>(nullable: false),
                    Mov_Genres = table.Column<byte>(nullable: false),
                    Mov_Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mov_Movie", x => x.Mov_MovieId);
                });

            migrationBuilder.CreateTable(
                name: "Use_User",
                columns: table => new
                {
                    Use_UserId = table.Column<Guid>(nullable: false),
                    Use_FirtName = table.Column<string>(nullable: true),
                    Use_SecondName = table.Column<string>(nullable: true),
                    Use_Email = table.Column<string>(nullable: true),
                    Use_Password = table.Column<string>(nullable: true),
                    Use_IsAdministrator = table.Column<bool>(nullable: false),
                    Use_Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Use_User", x => x.Use_UserId);
                });

            migrationBuilder.CreateTable(
                name: "Acm_ActorMovie",
                columns: table => new
                {
                    Acm_ActorMovieId = table.Column<Guid>(nullable: false),
                    Mov_MovieId = table.Column<Guid>(nullable: false),
                    Act_ActorId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acm_ActorMovie", x => x.Acm_ActorMovieId);
                    table.ForeignKey(
                        name: "FK_Acm_ActorMovie_Act_Actor_Act_ActorId",
                        column: x => x.Act_ActorId,
                        principalTable: "Act_Actor",
                        principalColumn: "Act_ActorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Acm_ActorMovie_Mov_Movie_Mov_MovieId",
                        column: x => x.Mov_MovieId,
                        principalTable: "Mov_Movie",
                        principalColumn: "Mov_MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MoviesClassification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MovieId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Vote = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoviesClassification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MoviesClassification_Mov_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Mov_Movie",
                        principalColumn: "Mov_MovieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoviesClassification_Use_User_UserId",
                        column: x => x.UserId,
                        principalTable: "Use_User",
                        principalColumn: "Use_UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Acm_ActorMovie_Mov_MovieId",
                table: "Acm_ActorMovie",
                column: "Mov_MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Acm_ActorMovie_Act_ActorId_Mov_MovieId",
                table: "Acm_ActorMovie",
                columns: new[] { "Act_ActorId", "Mov_MovieId" });

            migrationBuilder.CreateIndex(
                name: "IX_MoviesClassification_MovieId",
                table: "MoviesClassification",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_MoviesClassification_UserId",
                table: "MoviesClassification",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Acm_ActorMovie");

            migrationBuilder.DropTable(
                name: "MoviesClassification");

            migrationBuilder.DropTable(
                name: "Act_Actor");

            migrationBuilder.DropTable(
                name: "Mov_Movie");

            migrationBuilder.DropTable(
                name: "Use_User");
        }
    }
}
