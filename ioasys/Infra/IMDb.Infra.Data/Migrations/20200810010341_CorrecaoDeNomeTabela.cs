﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDb.Infra.Data.Migrations
{
    public partial class CorrecaoDeNomeTabela : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoviesClassification_Mov_Movie_MovieId",
                table: "MoviesClassification");

            migrationBuilder.DropForeignKey(
                name: "FK_MoviesClassification_Use_User_UserId",
                table: "MoviesClassification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MoviesClassification",
                table: "MoviesClassification");

            migrationBuilder.RenameTable(
                name: "MoviesClassification",
                newName: "Moc_MovieClassification");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Moc_MovieClassification",
                newName: "Use_UserId");

            migrationBuilder.RenameColumn(
                name: "MovieId",
                table: "Moc_MovieClassification",
                newName: "Mov_MovieId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Moc_MovieClassification",
                newName: "Moc_MovieClassificationId");

            migrationBuilder.RenameIndex(
                name: "IX_MoviesClassification_UserId",
                table: "Moc_MovieClassification",
                newName: "IX_Moc_MovieClassification_Use_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_MoviesClassification_MovieId",
                table: "Moc_MovieClassification",
                newName: "IX_Moc_MovieClassification_Mov_MovieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Moc_MovieClassification",
                table: "Moc_MovieClassification",
                column: "Moc_MovieClassificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Moc_MovieClassification_Mov_Movie_Mov_MovieId",
                table: "Moc_MovieClassification",
                column: "Mov_MovieId",
                principalTable: "Mov_Movie",
                principalColumn: "Mov_MovieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Moc_MovieClassification_Use_User_Use_UserId",
                table: "Moc_MovieClassification",
                column: "Use_UserId",
                principalTable: "Use_User",
                principalColumn: "Use_UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Moc_MovieClassification_Mov_Movie_Mov_MovieId",
                table: "Moc_MovieClassification");

            migrationBuilder.DropForeignKey(
                name: "FK_Moc_MovieClassification_Use_User_Use_UserId",
                table: "Moc_MovieClassification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Moc_MovieClassification",
                table: "Moc_MovieClassification");

            migrationBuilder.RenameTable(
                name: "Moc_MovieClassification",
                newName: "MoviesClassification");

            migrationBuilder.RenameColumn(
                name: "Use_UserId",
                table: "MoviesClassification",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "Mov_MovieId",
                table: "MoviesClassification",
                newName: "MovieId");

            migrationBuilder.RenameColumn(
                name: "Moc_MovieClassificationId",
                table: "MoviesClassification",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_Moc_MovieClassification_Use_UserId",
                table: "MoviesClassification",
                newName: "IX_MoviesClassification_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Moc_MovieClassification_Mov_MovieId",
                table: "MoviesClassification",
                newName: "IX_MoviesClassification_MovieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MoviesClassification",
                table: "MoviesClassification",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MoviesClassification_Mov_Movie_MovieId",
                table: "MoviesClassification",
                column: "MovieId",
                principalTable: "Mov_Movie",
                principalColumn: "Mov_MovieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MoviesClassification_Use_User_UserId",
                table: "MoviesClassification",
                column: "UserId",
                principalTable: "Use_User",
                principalColumn: "Use_UserId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
