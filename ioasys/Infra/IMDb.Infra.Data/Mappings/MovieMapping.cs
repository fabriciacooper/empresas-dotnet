﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infra.Data.Mappings
{
    public class MovieMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Mov_MovieId");

            builder.Property(c => c.Name)
                .HasColumnName("Mov_Name");

            builder.Property(c => c.Director)
                .HasColumnName("Mov_Director");

            builder.Property(c => c.MovieRelease)
                .HasColumnName("Mov_MovieRelease");

            builder.Property(c => c.Genres)
                .HasColumnName("Mov_Genres");

            builder.Property(c => c.Status)
               .HasColumnName("Mov_Status");

            builder.ToTable("Mov_Movie");
        }
    }
}
