﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infra.Data.Mappings
{
    public class MovieClassificationMapping : IEntityTypeConfiguration<MovieClassification>
    {
        public void Configure(EntityTypeBuilder<MovieClassification> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Moc_MovieClassificationId");

            builder.Property(c => c.MovieId)
                .HasColumnName("Mov_MovieId");
            builder.HasOne(x => x.Movie)
                .WithMany(x => x.MoviesClassification)
                .HasForeignKey(x => x.MovieId);

            builder.Property(c => c.UserId)
                .HasColumnName("Use_UserId");
            builder.HasOne(x => x.User)
                .WithMany(x => x.MoviesClassification)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Moc_MovieClassification");
        }
    }
}
