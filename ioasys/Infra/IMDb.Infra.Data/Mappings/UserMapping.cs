﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infra.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Use_UserId");

            builder.Property(c => c.FirstName)
                .HasColumnName("Use_FirtName");

            builder.Property(c => c.SecondName)
                .HasColumnName("Use_SecondName");

            builder.Property(c => c.Email)
                .HasColumnName("Use_Email");

            builder.Property(c => c.Password)
                .HasColumnName("Use_Password");

            builder.Property(c => c.Role)
               .HasColumnName("Use_Role");

            builder.Property(c => c.Status)
                .HasColumnName("Use_Status");

            builder.ToTable("Use_User");
        }
    }
}
