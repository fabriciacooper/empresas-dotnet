﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infra.Data.Mappings
{
    public class ActorMapping : IEntityTypeConfiguration<Actor>
    {
        public void Configure(EntityTypeBuilder<Actor> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Act_ActorId");

            builder.Property(c => c.Name)
                .HasColumnName("Act_Name");

            builder.Property(c => c.Age)
                .HasColumnName("Act_Age");

            builder.ToTable("Act_Actor");
        }
    }
}
