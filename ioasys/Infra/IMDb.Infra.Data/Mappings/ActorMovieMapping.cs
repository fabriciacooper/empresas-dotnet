﻿using IMDb.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Infra.Data.Mappings
{
    public class ActorMovieMapping : IEntityTypeConfiguration<ActorMovie>
    {
        public void Configure(EntityTypeBuilder<ActorMovie> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .HasColumnName("Acm_ActorMovieId");

            builder.Property(c => c.MovieId)
                .HasColumnName("Mov_MovieId");
            builder.HasOne(x => x.Movie)
                .WithMany(x => x.ActorMovie)
                .HasForeignKey(x => x.MovieId);

            builder.Property(c => c.ActorId)
                .HasColumnName("Act_ActorId");
            builder.HasOne(x => x.Actor)
                .WithMany(x => x.ActorMovie)
                .HasForeignKey(x => x.ActorId);

            builder.HasIndex(x => new {x.ActorId, x.MovieId});

            builder.ToTable("Acm_ActorMovie");
        }
    }
}
