﻿using IMDb.Domain.Models;
using IMDb.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace IMDb.Infra.Data.Context
{
    public class IMDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<ActorMovie> ActorMovies { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieClassification> MoviesClassification { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ActorMapping());
            modelBuilder.ApplyConfiguration(new ActorMovieMapping());
            modelBuilder.ApplyConfiguration(new MovieMapping());
            modelBuilder.ApplyConfiguration(new UserMapping());
            modelBuilder.ApplyConfiguration(new MovieClassificationMapping());

            base.OnModelCreating(modelBuilder);
        }

        public IMDbContext(DbContextOptions<IMDbContext> options) : base(options)
        {

        }

        public IDbContextTransaction Transaction { get; private set; }

        public IDbContextTransaction InitializeTransaction()
        {
            if (Transaction == null) 
                Transaction = this.Database.BeginTransaction();
            return Transaction;
        }

        private void RollBack()
        {
            if (Transaction != null)
            {
                Transaction.Rollback();
            }
        }
        private void Commit()
        {
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction.Dispose();
                Transaction = null;
            }
        }

        private void Save()
        {
            try
            {
                ChangeTracker.DetectChanges();
                SaveChanges();
            }
            catch (Exception ex)
            {
                RollBack();
                throw new Exception(ex.Message);
            }
        }

        public void SendChanges()
        {
            Save();
            Commit();
        }        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() + "//Config")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                .Build();
            optionsBuilder.EnableSensitiveDataLogging(false);
            optionsBuilder.UseNpgsql(config.GetConnectionString("DefaultConnection"), opts => { });
            base.OnConfiguring(optionsBuilder);
        }
    }
}
