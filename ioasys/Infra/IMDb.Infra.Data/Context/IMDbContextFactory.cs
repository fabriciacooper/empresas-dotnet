﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace IMDb.Infra.Data.Context
{
    public class IMDbContextFactory : IDesignTimeDbContextFactory<IMDbContext>
    {
        public IMDbContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() + "//Config")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                .Build();
            
            var optionsBuilder = new DbContextOptionsBuilder<IMDbContext>();
            optionsBuilder.UseNpgsql(config.GetConnectionString("DefaultConnection"), opts => { });

            return new IMDbContext(optionsBuilder.Options);
        }
    }
}
